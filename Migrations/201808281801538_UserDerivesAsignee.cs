namespace CodeFirstTPT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserDerivesAsignee : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Users");
            CreateTable(
                "dbo.Assignees",
                c => new
                    {
                        AssigneeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AssigneeId);
            
            AddColumn("dbo.Users", "AssigneeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "UserId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Users", "AssigneeId");
            CreateIndex("dbo.Users", "AssigneeId");
            AddForeignKey("dbo.Users", "AssigneeId", "dbo.Assignees", "AssigneeId");
            DropColumn("dbo.Users", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Name", c => c.String(nullable: false));
            DropForeignKey("dbo.Users", "AssigneeId", "dbo.Assignees");
            DropIndex("dbo.Users", new[] { "AssigneeId" });
            DropPrimaryKey("dbo.Users");
            AlterColumn("dbo.Users", "UserId", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Users", "AssigneeId");
            DropTable("dbo.Assignees");
            AddPrimaryKey("dbo.Users", "UserId");
        }
    }
}
