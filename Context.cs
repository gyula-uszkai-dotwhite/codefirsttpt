﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace CodeFirstTPT
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<PostIt> PostIts { get; set; }
        public DbSet<Folder> Folders { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .Property(t => t.Name)
                .IsRequired();

            modelBuilder.Entity<Assignee>().ToTable("Assignees");

            modelBuilder.Entity<User>()
                .ToTable("Users")
                .Property(t => t.CNP).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsOptional();

            modelBuilder.Entity<Group>()
                .ToTable("Groups");
        }
    }
}
