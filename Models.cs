﻿using System.Collections.Generic;

namespace CodeFirstTPT
{
    class Models
    {
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Name { get; set; }

        public virtual List<Post> Posts { get; set; }
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }
    }

    public class User : Assignee
    {
        public int CNP { get; set; }
        public virtual Group Group { get; set; }
    }

    public class Group : Assignee
    {
        public string RandomProperty { get; set; }
    }

    public class PostIt
    {
        public int PostItId { get; set; }
        public string Comment { get; set; }
        public virtual Assignee Responsible { get; set; }
    }

    public abstract class Assignee
    {
        public int AssigneeId { get; set; }
        public string Name { get; set; }
    }

    public class Folder
    {
        public int FolderId { get; set; }
        public string Info { get; set; }
        public virtual ICollection<PostIt> PostIts { get; set; }
    }
}
