namespace CodeFirstTPT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserNameIsRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Name", c => c.String());
        }
    }
}
