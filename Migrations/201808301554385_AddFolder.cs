namespace CodeFirstTPT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFolder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Folders",
                c => new
                    {
                        FolderId = c.Int(nullable: false, identity: true),
                        Info = c.String(),
                    })
                .PrimaryKey(t => t.FolderId);
            
            AddColumn("dbo.PostIts", "Folder_FolderId", c => c.Int());
            CreateIndex("dbo.PostIts", "Folder_FolderId");
            AddForeignKey("dbo.PostIts", "Folder_FolderId", "dbo.Folders", "FolderId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostIts", "Folder_FolderId", "dbo.Folders");
            DropIndex("dbo.PostIts", new[] { "Folder_FolderId" });
            DropColumn("dbo.PostIts", "Folder_FolderId");
            DropTable("dbo.Folders");
        }
    }
}
