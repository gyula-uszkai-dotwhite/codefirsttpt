namespace CodeFirstTPT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPostIt : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostIts",
                c => new
                    {
                        PostItId = c.Int(nullable: false, identity: true),
                        Comment = c.String(),
                        Responsible_AssigneeId = c.Int(),
                    })
                .PrimaryKey(t => t.PostItId)
                .ForeignKey("dbo.Assignees", t => t.Responsible_AssigneeId)
                .Index(t => t.Responsible_AssigneeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostIts", "Responsible_AssigneeId", "dbo.Assignees");
            DropIndex("dbo.PostIts", new[] { "Responsible_AssigneeId" });
            DropTable("dbo.PostIts");
        }
    }
}
