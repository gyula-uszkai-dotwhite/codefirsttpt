﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CodeFirstTPT
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                //var group = new Group()
                //{
                //    Name = "First group",
                //    RandomProperty = "some stuff"
                //};

                //db.Groups.Add(group);
                //db.SaveChanges();

                //var user = new User()
                //{
                //    Name = "user1",
                //};
                //db.Users.Add(user);

                //var user2 = new User()
                //{
                //    Name = "user2",
                //    Group = group
                //};
                //db.Users.Add(user2);

                //db.SaveChanges();

                //var post1 = new PostIt()
                //{
                //    Comment = "this has a user",
                //    Responsible = user2
                //};

                //db.PostIts.Add(post1);

                //var post2 = new PostIt()
                //{
                //    Comment = "this has a group",
                //    Responsible = new Group() { Name = "random lonely group" }
                //};


                //db.PostIts.Add(post2);

                //db.SaveChanges();

                //var postitList = db.PostIts.ToList();



                //var folder = new Folder()
                //{
                //    Info = "some folder",
                //    PostIts = postitList
                //};

                //db.Folders.Add(folder);

                //db.SaveChanges();



                //Console.WriteLine("Press any key to exit...");
                //Console.ReadKey();
            }

            IList<Folder> res;
            using (var db2 = new BloggingContext())
            {
                var folders = db2.Folders
                    .Include(f => f.PostIts.Select(p => p.Responsible))
                    .ToList();

                Console.WriteLine("Press any key to exit...");
                res = folders.ToList();
            }

            var b = res.First().PostIts.First().Responsible;
            Console.WriteLine("Is responsible loaded:" + b == null);
        }

        static void Test1()
        {
            using (var db = new BloggingContext())
            {
                // Create and save a new Blog
                Console.Write("Enter a name for a new Blog: ");
                var name = Console.ReadLine();

                var blog = new Blog { Name = name };
                db.Blogs.Add(blog);
                db.SaveChanges();

                // Display all Blogs from the database
                var query = from b in db.Blogs
                            orderby b.Name
                            select b;

                Console.WriteLine("All blogs in the database:");
                foreach (var item in query)
                {
                    Console.WriteLine(item.Name);
                }

                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
        }
    }
}
