namespace CodeFirstTPT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "CNP", c => c.Int());
            DropColumn("dbo.Groups", "GroupId");
            DropColumn("dbo.Users", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "UserId", c => c.Int(nullable: false));
            AddColumn("dbo.Groups", "GroupId", c => c.Int(nullable: false));
            DropColumn("dbo.Users", "CNP");
        }
    }
}
