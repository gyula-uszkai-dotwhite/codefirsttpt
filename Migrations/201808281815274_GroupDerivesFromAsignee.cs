namespace CodeFirstTPT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupDerivesFromAsignee : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "Group_GroupId", "dbo.Groups");
            RenameColumn(table: "dbo.Users", name: "Group_GroupId", newName: "Group_AssigneeId");
            RenameIndex(table: "dbo.Users", name: "IX_Group_GroupId", newName: "IX_Group_AssigneeId");
            DropPrimaryKey("dbo.Groups");
            AddColumn("dbo.Groups", "AssigneeId", c => c.Int(nullable: false));
            AddColumn("dbo.Groups", "RandomProperty", c => c.String());
            AlterColumn("dbo.Groups", "GroupId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Groups", "AssigneeId");
            CreateIndex("dbo.Groups", "AssigneeId");
            AddForeignKey("dbo.Groups", "AssigneeId", "dbo.Assignees", "AssigneeId");
            AddForeignKey("dbo.Users", "Group_AssigneeId", "dbo.Groups", "AssigneeId");
            DropColumn("dbo.Groups", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Groups", "Name", c => c.String());
            DropForeignKey("dbo.Users", "Group_AssigneeId", "dbo.Groups");
            DropForeignKey("dbo.Groups", "AssigneeId", "dbo.Assignees");
            DropIndex("dbo.Groups", new[] { "AssigneeId" });
            DropPrimaryKey("dbo.Groups");
            AlterColumn("dbo.Groups", "GroupId", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Groups", "RandomProperty");
            DropColumn("dbo.Groups", "AssigneeId");
            AddPrimaryKey("dbo.Groups", "GroupId");
            RenameIndex(table: "dbo.Users", name: "IX_Group_AssigneeId", newName: "IX_Group_GroupId");
            RenameColumn(table: "dbo.Users", name: "Group_AssigneeId", newName: "Group_GroupId");
            AddForeignKey("dbo.Users", "Group_GroupId", "dbo.Groups", "GroupId");
        }
    }
}
